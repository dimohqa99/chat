import { Controller, Get, Render } from 'routing-controllers';

@Controller()
export class ViewController {
  @Get('*')
  @Render('main.pug')
  render() {
    return {
      title: 'Chat',
    };
  }
}
