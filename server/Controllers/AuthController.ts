import {
  Body,
  ForbiddenError,
  HttpError,
  NotFoundError,
  Post,
  Ctx,
  JsonController,
} from 'routing-controllers';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { Context } from 'koa';
import RefreshTokenService from '../Services/RefreshTokenService';
import { UserModel } from '../Models/UserModel';

const saltRounds = 12;

@JsonController('/auth')
export class AuthController {
  @Post('/register')
  async register(
    @Body()
    body: {
      email: string;
      password: string;
      firstName: string;
      lastName: string;
    },
  ) {
    const { email, password, firstName, lastName } = body;

    const user = await UserModel.findOne({ email });

    if (user) {
      throw new HttpError(400, 'User with this email already exists');
    }

    const hashedPassword = await bcrypt.hash(password, saltRounds);

    const createdUser = new UserModel({
      email,
      password: hashedPassword,
      firstName,
      lastName,
    });

    await createdUser.save();

    return { status: 200 };
  }

  @Post('/login')
  async login(
    @Body() body: { email: string; password: string },
    @Ctx() ctx: Context,
  ) {
    const { email, password } = body;

    const user = await UserModel.findOne({ email });

    if (!user) {
      throw new NotFoundError('User was not found');
    }

    const passwordIsCorrect = await bcrypt.compare(password, user.password);

    if (!passwordIsCorrect) {
      throw new ForbiddenError('Password is not correct');
    }

    const token = jwt.sign({}, 'secret', {
      expiresIn: '1h',
    });

    ctx.cookies.set('token', token, {
      httpOnly: true,
    });

    const refreshToken = RefreshTokenService.random();

    await RefreshTokenService.generate(user.id, refreshToken).save();

    ctx.cookies.set('refreshToken', refreshToken, {
      httpOnly: true,
    });

    return { status: 200 };
  }
}
