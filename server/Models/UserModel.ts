import mongoose, { Schema, model } from 'mongoose';

type User = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
};

const UserSchema = new Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
});

export const UserModel = model<User & mongoose.Document>('User', UserSchema);
