import mongoose, { Schema, model } from 'mongoose';

type RefreshToken = {
  user: Schema.Types.ObjectId;
  token: string;
  expires: Date;
  created: Date;
  revoked: Date;
  replacedByToken: string;
};

const RefreshTokenSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  token: String,
  expires: Date,
  created: { type: Date, default: Date.now },
  revoked: Date,
  replacedByToken: String,
});

// eslint-disable-next-line func-names
RefreshTokenSchema.virtual('isExpired').get(function () {
  return Date.now() >= this.expires;
});

// eslint-disable-next-line func-names
RefreshTokenSchema.virtual('isActive').get(function () {
  return !this.revoked && !this.isExpired;
});

export const RefreshTokenModel = model<RefreshToken & mongoose.Document>(
  'RefreshToken',
  RefreshTokenSchema,
);
