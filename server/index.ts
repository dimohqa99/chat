import 'reflect-metadata';
import serve from 'koa-static';
import bodyParser from 'koa-bodyparser';
import Router from 'koa-router';
import views from 'koa-views';
import open from 'open';
import logger from 'koa-logger';
import mongoose from 'mongoose';
import { resolve } from 'path';
import { createKoaServer } from 'routing-controllers';

import { config } from './config';

const { port, mongoUri } = config;

const devAppUrl = `localhost:${port}`;

(async () => {
  const app = createKoaServer({
    controllers: [`${__dirname}/Controllers/*.ts`],
  });

  app.use(logger());

  const router = new Router();

  app.use(bodyParser());

  app.use(router.routes()).use(router.allowedMethods());

  router.get('/test', ctx => {
    ctx.body = 'Test ok';
  });

  app.use(serve(resolve(__dirname, '..', 'dist')));

  app.use(
    views(resolve(__dirname, 'views'), {
      extension: 'pug',
      map: { pug: 'pug' },
    }),
  );

  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  });

  app.listen(port, () => {
    console.log(`Application running on a port: ${port}`);
    open(devAppUrl);
  });
})();
