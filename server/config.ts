import dotenv from 'dotenv';

dotenv.config();

const port = 9000;
const mongoUri = process.env.URI;

export const config = {
  port,
  mongoUri,
};
