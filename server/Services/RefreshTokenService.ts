import { randomBytes } from 'crypto';
import { RefreshTokenModel } from '../Models/RefreshTokenModel';

class RefreshTokenService {
  random() {
    return randomBytes(40).toString('hex');
  }

  generate(id: string, token: string) {
    return new RefreshTokenModel({
      user: id,
      token,
      expires: new Date(Date.now() + 7 * 24 * 60 * 60 * 1000),
    });
  }
}

export default new RefreshTokenService();
