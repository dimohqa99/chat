const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');

module.exports = {
  target: 'node',
  entry: {
    main: path.resolve(__dirname, 'server', 'index.ts'),
  },
  output: {
    path: path.join(__dirname, 'dist', 'server'),
    filename: 'index.js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: [path.resolve(__dirname, 'node_modules')],
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({ 'global.GENTLY': false }),
    new webpack.HotModuleReplacementPlugin(),
  ],
  externals: [nodeExternals()],
  resolve: {
    extensions: ['.ts', '.js'],
  },
};
